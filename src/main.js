import { createApp } from "vue";
import App from "./App.vue";

import("bootstrap/scss/bootstrap-reboot.scss");
import("bootstrap/scss/bootstrap-grid.scss");
import("./assets/scss/style.scss");

createApp(App).mount("#app");
